#include <pthread.h>
#include <stdio.h>

const unsigned long count_to = 1000000000;
unsigned long counter = 0;

const unsigned int number_of_threads = 4;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void* thread_function(void* unused){
	printf("Thread started!\n");

	for(;;){
		pthread_mutex_lock(&mutex);
		
		if(counter >= count_to){
			pthread_mutex_unlock(&mutex);
			printf("Thread finished!\n");
			return NULL;
		}

		counter++;

		pthread_mutex_unlock(&mutex);
	}
}

int main(){
	printf("Starting %u threads to do the counting...\n", number_of_threads);

	pthread_t threads[number_of_threads];

	for(unsigned int i=0; i<number_of_threads; i++){
		pthread_create(&(threads[i]), NULL, thread_function, NULL);
	}

	for(unsigned int i=0; i<number_of_threads; i++){
		pthread_join(threads[i], NULL);
	}

	printf("\nDone counting to %lu out of %lu\n", counter, count_to);

	return 1;
}
