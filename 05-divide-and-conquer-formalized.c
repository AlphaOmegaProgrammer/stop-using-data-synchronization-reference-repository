#include <assert.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

struct thread_work{
	unsigned long start;
	unsigned long limit;

	unsigned long counter;
};

const unsigned long count_to = 1000000000;
const unsigned int number_of_threads = 4;

void* thread_function(void* arg){
	struct thread_work *work = (struct thread_work*)arg;

	printf("Thread started!\n");

	for(;work->start + work->counter < work->limit; work->counter++);

	printf("Thread finished!\n");
	return (void*)work;
}

int main(){
	assert(count_to % number_of_threads == 0);

	printf("Starting %u threads to do the counting...\n", number_of_threads);

	pthread_t threads[number_of_threads];

	for(unsigned int i=0; i<number_of_threads; i++){
		struct thread_work *work = malloc(sizeof(struct thread_work));

		work->start = (i * (count_to / number_of_threads));
		work->limit = work->start + (count_to / number_of_threads);

		work->counter = 0;

		pthread_create(&(threads[i]), NULL, thread_function, work);
	}

	unsigned long total = 0;

	for(unsigned int i=0; i<number_of_threads; i++){
		struct thread_work *work;

		pthread_join(threads[i], (void**)&work);

		total += work->counter;

		free(work);
	}

	printf("\nDone counting to %lu out of %lu\n", total, count_to);

	return 1;
}
