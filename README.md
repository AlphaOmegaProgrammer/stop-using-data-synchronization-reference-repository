# Stop Using Data Synchronization

This repository holds reference code for my rant [Stop Using Data Synchronization](https://catgirls.meme/rants/stop-using-data-synchronization/) (not yet live).

## Building

To follow along, you will need Linux and the following programs:
 * `make`
 * `gcc` (or another C compiler that can compile atomics)
 * `time`
 * `rm` (for `make clean`)

You can compile each example in many different ways. These are all of the ways you can compile 01-control.c:
 * `make 01`
 * `make control`
 * `make 01-control`
 * `make bin/01-control`

**Compiling with optimization enabled will likely cause these programs to behave in unintended ways**

## Executing

Programs `01-control`, `02-mutex`, and `03-simple-atomic` should execute within a few seconds. If they do not, go through each example program and adjust the `count_to` limit in each. Limits in some example programs may need to be much higher or lower than others to execute in a reasonable time and extrapolate the results to compare between the different approaches.

When executing these programs, do so with the `time` command (`time ./bin/01-control`). The outputs will display 3 different times, which mean different things:
 * **real** - Real time is the amount of real world time that passed while the program was executing. This is ultimately the most important timing.
 * **user** - User time is the amount of CPU time the program used. For example, if 4 threads used 50% of a CPU core for 1 second each, this timing would be 2 seconds.
 * **sys** - System time is the amount of CPU time used by the kernel, doing this like syscalls or context switching. This number should be as low as possible.
