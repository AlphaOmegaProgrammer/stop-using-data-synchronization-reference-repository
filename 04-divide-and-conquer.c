#include <assert.h>
#include <pthread.h>
#include <stdio.h>

const unsigned long count_to = 1000000000;
const unsigned int number_of_threads = 4;
const unsigned long per_thread_count_to = count_to / number_of_threads;


void* thread_function(void* unused){
	unsigned long counter = 0;

	printf("Thread started!\n");

	for(;counter < per_thread_count_to; counter++);

	printf("Thread finished!\n");
	return (void*)counter;
}

int main(){
	assert(count_to % number_of_threads == 0);

	printf("Starting %u threads to do the counting...\n", number_of_threads);

	pthread_t threads[number_of_threads];

	for(unsigned int i; i<number_of_threads; i++){
		pthread_create(&(threads[i]), NULL, thread_function, NULL);
	}

	unsigned long total = 0;

	for(unsigned int i=0; i<number_of_threads; i++){
		unsigned int returned_value;

		pthread_join(threads[i], (void**)&returned_value);

		total += returned_value;
	}

	printf("\nDone counting to %lu out of %lu\n", total, count_to);

	return 1;
}
