#include <stdio.h>

const unsigned long count_to = 1000000000;
unsigned long counter = 0;

int main(){
	printf("Starting counting...\n\n");

	for(;counter < count_to; counter++);

	printf("Finished counting to %lu out of %lu\n", counter, count_to);

	return 1;
}
