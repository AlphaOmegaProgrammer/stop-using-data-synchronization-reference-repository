#include <assert.h>
#include <inttypes.h>
#include <pthread.h>
#include <stdatomic.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>


struct thread_data{
	uint64_t start;
	uint64_t limit;
};


const uint64_t count_to = 1000000000;
const unsigned int number_of_threads = 4;

// Must be a power of 2
#define hashmap_size 4096
const unsigned int hashmap_and_by = hashmap_size - 1;
_Atomic(uint64_t) hashmap[hashmap_size];

void* thread_function(void* arg){
	struct thread_data *data = (struct thread_data*)arg;

	printf("Thread started!\n");

	for(;data->start < data->limit; data->start++){
		unsigned int hashmap_index = data->start & hashmap_and_by;
		uint64_t local_counter = atomic_load(&hashmap[hashmap_index]);

		while(!atomic_compare_exchange_strong_explicit(
			&hashmap[hashmap_index],
			&local_counter,
			local_counter + 1,
			memory_order_relaxed,
			memory_order_relaxed
		));
	}

	printf("Thread finished!\n");

	free(data);

	return NULL;
}

int main(){
	assert(count_to % number_of_threads == 0);
	assert((hashmap_size & (hashmap_size - 1)) == 0); // Must be power of 2

	for(unsigned i=0; i<hashmap_size; i++){
		atomic_store(&hashmap[i], 0);
	}

	printf("Starting %u threads to do the counting...\n", number_of_threads);

	pthread_t threads[number_of_threads];

	for(unsigned int i=0; i<number_of_threads; i++){
		struct thread_data *data = malloc(sizeof(struct thread_data));

		data->start = (i * (count_to / number_of_threads));
		data->limit = data->start + (count_to / number_of_threads);

		pthread_create(&(threads[i]), NULL, thread_function, data);
	}

	for(unsigned int i=0; i<number_of_threads; i++){
		pthread_join(threads[i], NULL);
	}

	uint64_t total = 0;

	for(unsigned int i=0; i<hashmap_size; total += hashmap[i], i++);

	printf("\nDone counting to %"PRIu64" out of %lu\n", total, count_to);

	return 1;
}
