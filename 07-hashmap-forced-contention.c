#include <assert.h>
#include <pthread.h>
#include <stdatomic.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>


// Remember that hashmaps are backed by a linked list
// on hash collisions.
struct hashmap_entry_node{
	unsigned int thread_id;
	unsigned int counter;
	struct hashmap_entry_node *next;
};


// Unfortunately, there is no lock-free linked list implementation.
// Note that this lock is explicitly made to be 8 bytes, because
// x86_64 only has CMPXCHG8B and CMPXCHG16B, meaning that we can
// only compare exchange 8 bytes or 16 bytes.
struct hashmap_node{
	_Atomic uint64_t lock;
	struct hashmap_entry_node *entries;
};

struct thread_data{
	unsigned int thread_id;

	unsigned long start;
	unsigned long limit;
};


// Make sure this is a power of 2
#define hashmap_size 4096
const unsigned int hashmap_and_by = hashmap_size - 1;
struct hashmap_node hashmap[hashmap_size];

const unsigned long count_to = 1000000000;
const unsigned int number_of_threads = 4;

void* thread_function(void* arg){
	struct thread_data *data = (struct thread_data*)arg;

	printf("Thread %u started!\n", data->thread_id);

	for(;data->start < data->limit; data->start++){
		// Acquire the lock for the linked list backing the hashmap entry
		unsigned int hashmap_index = data->start & hashmap_and_by;
		while(atomic_exchange(&hashmap[hashmap_index].lock, 1) == 1);

		// Now find the correct entry for this thread
		struct hashmap_entry_node *node = hashmap[hashmap_index].entries;
		for(;node != NULL && node->thread_id != data->thread_id; node = node->next);

		// If there wasn't one, create one
		if(node == NULL){
			node = malloc(sizeof(struct hashmap_entry_node));

			node->thread_id = data->thread_id;
			node->counter = 0;
			node->next = hashmap[hashmap_index].entries;

			hashmap[hashmap_index].entries = node;
		}

		// Then increment the node
		node->counter++;

		// Now release the linked list lock
		atomic_store(&hashmap[hashmap_index].lock, 0);
	}

	printf("Thread %u finished!\n", data->thread_id);

	free(data);

	return NULL;
}

int main(){
	assert(count_to % number_of_threads == 0);

	for(unsigned i=0; i<hashmap_size; i++){
		atomic_store(&hashmap[i].lock, 0);
		hashmap[i].entries = NULL;
	}

	printf("Starting %u threads to do the counting...\n", number_of_threads);

	pthread_t threads[number_of_threads];

	for(unsigned int i=0; i<number_of_threads; i++){
		struct thread_data *data = malloc(sizeof(struct thread_data));

		data->thread_id = i;

		data->start = (i * (count_to / number_of_threads));
		data->limit = data->start + (count_to / number_of_threads);

		pthread_create(&(threads[i]), NULL, thread_function, data);
	}

	for(unsigned int i=0; i<number_of_threads; i++){
		pthread_join(threads[i], NULL);
	}

	unsigned long total = 0;

	for(unsigned int i=0; i<hashmap_size; i++){
		struct hashmap_entry_node *node = hashmap[i].entries;
		for(;node != NULL; total += node->counter, node = node->next);
	}

	printf("\nDone counting to %lu out of %lu\n", total, count_to);

	return 1;
}
