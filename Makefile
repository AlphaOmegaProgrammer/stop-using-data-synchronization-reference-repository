.PHONY: all build clean

all:
	@echo "You must specify target!"

clean:
	@$(RM) -rf bin/*
	@echo "bin/* has been emptied"


.PHONY: 01 control 01-control
01: bin/01-control
01-control: bin/01-control
control: bin/01-control

bin/01-control: 01-control.c 
	@$(CC) -Wall -Wextra -pedantic $(CFLAGS) $^ -o $@
	@echo "$@ has been compiled"


.PHONY: 02 mutex 02-mutex
02: bin/02-mutex
02-mutex: bin/02-mutex
mutex: bin/02-mutex

bin/02-mutex: 02-mutex.c
	@$(CC) -Wall -Wextra -pedantic -Wno-unused-parameter $(CFLAGS) $^ -o $@
	@echo "$@ has been compiled"


.PHONY: 03 atomic 03-atomic
03: bin/03-atomic
03-atomic: bin/03-atomic
atomic: bin/03-atomic

bin/03-atomic: 03-atomic.c
	@$(CC) -Wall -Wextra -pedantic -Wno-unused-parameter $(CFLAGS) $^ -o $@
	@echo "$@ has been compiled"


.PHONY: 04 divide-and-conquer 04-divide-and-conquer
04: bin/04-divide-and-conquer
04-divide-and-conquer: bin/04-divide-and-conquer
divide-and-conquer: bin/04-divide-and-conquer

bin/04-divide-and-conquer: 04-divide-and-conquer.c
	@$(CC) -Wall -Wextra -pedantic -Wno-unused-parameter $(CFLAGS) $^ -o $@
	@echo "$@ has been compiled"


.PHONY: 05 divide-and-conquer-formalized 05-divide-and-conquer-formalized
05: bin/05-divide-and-conquer-formalized
05-divide-and-conquer-formalized: bin/05-divide-and-conquer-formalized
divide-and-conquer-formalized: bin/05-divide-and-conquer-formalized

bin/05-divide-and-conquer-formalized: 05-divide-and-conquer-formalized.c
	@$(CC) -Wall -Wextra -pedantic $(CFLAGS) $^ -o $@
	@echo "$@ has been compiled"


.PHONY: 06 hashmap 06-hashmap
06: bin/06-hashmap
06-hashmap: bin/06-hashmap
hashmap: bin/06-hashmap

bin/06-hashmap: 06-hashmap.c
	@$(CC) -Wall -Wextra -pedantic $(CFLAGS) $^ -o $@
	@echo "$@ has been compiled"


.PHONY: 07 hashmap-forced-contention 07-hashmap-forced-contention
07: bin/07-hashmap-forced-contention
07-hashmap-forced-contention: bin/07-hashmap-forced-contention
hashmap-forced-contention: bin/07-hashmap-forced-contention

bin/07-hashmap-forced-contention: 07-hashmap-forced-contention.c
	@$(CC) -Wall -Wextra -pedantic $(CFLAGS) $^ -o $@
	@echo "$@ has been compiled"
