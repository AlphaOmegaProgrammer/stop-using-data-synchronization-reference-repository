#include <assert.h>
#include <inttypes.h>
#include <pthread.h>
#include <stdatomic.h>
#include <stdint.h>
#include <stdio.h>

const uint64_t count_to = 1000000000;
_Atomic(uint64_t) counter;

const unsigned int number_of_threads = 1;
const uint64_t count_to_per_thread = count_to / number_of_threads;

void* thread_function(void* unused){
	printf("Thread started!\n");

	for(uint64_t start = 0; start < count_to_per_thread; start++){
		uint64_t local_counter = atomic_load(&counter);

		while(!atomic_compare_exchange_strong_explicit(
			&counter,
			&local_counter,
			local_counter + 1,
			memory_order_relaxed,
			memory_order_relaxed
		));
	}

	printf("Thread finished!\n");
	return NULL;
}

int main(){
	assert(count_to % number_of_threads == 0);

	atomic_init(&counter, 0);

	printf("Starting %u threads to do the counting...\n", number_of_threads);

	pthread_t threads[number_of_threads];

	for(unsigned int i=0; i<number_of_threads; i++){
		pthread_create(&(threads[i]), NULL, thread_function, NULL);
	}

	for(unsigned int i=0; i<number_of_threads; i++){
		pthread_join(threads[i], NULL);
	}

	printf("\nDone counting to %"PRIu64" out of %lu\n", counter, count_to);

	return 1;
}
